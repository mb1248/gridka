#include <chrono>
// #include <iomanip>
#include <cstdio>
// #include <limits>
#include <random>
#include <string>
#include <vector>
#include "mpi.h"

int samplesInsideCircle(int numSamples) {
  unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::mt19937 generator(seed);
  std::uniform_real_distribution<double> uniform(0.0,1.0);

  int counts = 0;

  for (int i = 0; i < numSamples; ++i) {
    double x = uniform(generator);
    double y = uniform(generator);

    if (x * x + y * y < 1.0) {
      ++counts;
    }
  }

  return counts;
}



int main(int argc, char* argv[]) {
  // Initialize the MPI environment
  MPI_Init(&argc, &argv);

  // Get the number of processes
  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  // Get the rank of the process
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int localSampels = 1'000'000;

  int counts = samplesInsideCircle(localSampels);

  // Print a hello world message
  std::printf("Hello from rank %d, my local pi = %lf\n", rank, 4.0 * counts / localSampels);

  MPI_Request request;

  int sum;
  MPI_Reduce(&counts, &sum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  if (rank == 0) {
    std::printf("Hello from rank %d, my global pi = %lf\n", rank, 4.0 * sum / (size * localSampels));
  }

  // Finalize the MPI environment.
  MPI_Finalize();
}
