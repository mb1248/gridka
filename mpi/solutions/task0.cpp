#include <cstdio>
#include "mpi.h"


int main(int argc, char* argv[]) {
  // Initialize the MPI environment
  MPI_Init(&argc, &argv);

  // Get the number of processes
  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  // Get the rank of the process
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // Print a hello world message
  std::printf("Hello world from rank %d\n", rank);

  MPI_Barrier(MPI_COMM_WORLD);

  if (rank == 0) {
    std::printf("We are a group of %d ranks\n", size);
  }

  // Finalize the MPI environment.
  MPI_Finalize();
}
