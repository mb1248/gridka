#include <chrono>
// #include <iomanip>
#include <cstdio>
// #include <limits>
#include <random>
#include <string>
#include <vector>
#include "mpi.h"

int samplesInsideCircle(int numSamples) {
  unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::mt19937 generator(seed);
  std::uniform_real_distribution<double> uniform(0.0,1.0);

  int counts = 0;

  for (int i = 0; i < numSamples; ++i) {
    double x = uniform(generator);
    double y = uniform(generator);

    if (x * x + y * y < 1.0) {
      ++counts;
    }
  }

  return counts;
}

int main(int argc, char* argv[]) {
  // Initialize the MPI environment
  MPI_Init(&argc, &argv);

  // Get the number of processes
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // Get the rank of the process
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int localSampels = 1'000'000;

  int counts = samplesInsideCircle(localSampels);

  // Print  a hello world message
  std::printf("Hello from rank %d, my local pi = %lf\n", rank, (4.0 * counts) / localSampels );

  if (rank == 0) {
    int sum = counts;
    for (int i = 1; i < world_size; ++i) {
      MPI_Recv(&counts, 1, MPI_INT, i, 42, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      sum += counts;
    }
    // Print a hello world message
    std::printf("Hello from rank %d, my local pi = %lf\n", rank, (4.0 * sum) / (world_size * localSampels));
  } else {
    MPI_Send(&counts, 1, MPI_INT, 0, 42, MPI_COMM_WORLD);
  }

  // Finalize the MPI environment.
  MPI_Finalize();
}
