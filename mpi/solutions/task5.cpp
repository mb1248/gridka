#include <cstdio>
#include "mpi.h"
#include "omp.h"


int main(int argc, char* argv[]) {
  // Initialize the MPI environment
  MPI_Init(&argc, &argv);

  // Get the number of processes
  int size;
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  // Get the rank of the process
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  #pragma omp parallel
  {
    std::printf("Task %d on rank %d.\n", omp_get_thread_num(), rank);
  }
  // Finalize the MPI environment.
  MPI_Finalize();
}
