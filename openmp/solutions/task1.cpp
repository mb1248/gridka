#include "omp.h"
#include <cstdio>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

// wait for 3 x 1000 ms
void function_1() {
  for (int i = 0; i < 3; ++i) {
    std::printf("Function 1 (i = %d, thread = %d)\n", i, omp_get_thread_num());
    std::this_thread::sleep_for(1000ms);
  }
}

// wait for 4 x 500 ms
void function_2() {
  for (int i = 0; i < 4; ++i) {
    std::this_thread::sleep_for(500ms);
    std::printf("Function 2 (i = %d, thread = %d)\n", i, omp_get_thread_num());
  }
}

// wait for 4 x 250 ms
void function_3() {
  for (int i = 0; i < 4; ++i) {
    std::this_thread::sleep_for(250ms);
    std::printf("Function 3 (i = %d, thread = %d)\n", i, omp_get_thread_num());
  }
}

int main() {
  #pragma omp parallel sections
  {
    #pragma omp section
    function_1();

    #pragma omp section
    function_2();

    #pragma omp section
    function_3();
  }
}