#include <chrono>
#include <iomanip>
#include <iostream>
#include <limits>
#include <random>
#include <string>
#include <vector>
#include <omp.h>

const int numTotalSamples = 10'000'000; // 10 million

unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
std::mt19937 generator(seed);                            // initialize
std::uniform_real_distribution<double> uniform(0.0,1.0); // the random number
                                                         // generator


void sequentialPi() {
  int counts = 0;
  for (int i = 0; i < numTotalSamples; ++i) {
    double x = uniform(generator); // generate a random number [0,1]
    double y = uniform(generator); // generate a random number [0,1]

    if (x * x + y * y < 1.0) {
      ++counts;
    }
  }

  double approxPi = (4.0 * counts) / numTotalSamples;

  std::cout << "Sequential version:\n";
  std::cout << "number of samples: " << numTotalSamples << "\n";
  std::cout << "real Pi: 3.141592653589...\n";
  std::cout << "approx.: " << std::setprecision(12) << approxPi << "\n";
}


void parallelPi() {
  int counts = 0;
  #pragma omp parallel for reduction(+: counts)
  for (int i = 0; i < numTotalSamples; ++i) {
    double x = uniform(generator);
    double y = uniform(generator);

    if (x * x + y * y < 1.0) {
      ++counts;
    }
  }

  double approxPi = (4.0 * counts) / numTotalSamples;

  std::cout << "Parallel version:\n";
  std::cout << "number of samples: " << numTotalSamples << "\n";
  std::cout << "real Pi: 3.141592653589...\n";
  std::cout << "approx.: " << std::setprecision(12) << approxPi << "\n";
}

void parallelPi2() {
  int counts = 0;
  #pragma omp parallel
  {
    unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 generator(seed);                            // initialize
    std::uniform_real_distribution<double> uniform(0.0,1.0); // the random number
                                                             // generator

    #pragma omp for reduction(+: counts)
    for (int i = 0; i < numTotalSamples; ++i) {
      double x = uniform(generator);
      double y = uniform(generator);

      if (x * x + y * y < 1.0) {
        ++counts;
      }
    }
  }

  double approxPi =( 4.0 * counts) / numTotalSamples;

  std::cout << "Parallel version 2:\n";
  std::cout << "number of samples: " << numTotalSamples << "\n";
  std::cout << "real Pi: 3.141592653589...\n";
  std::cout << "approx.: " << std::setprecision(12) << approxPi << "\n";
}

int samplesInsideCircle(int numSamples) {
  unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::mt19937 generator(seed);
  std::uniform_real_distribution<double> uniform(0.0,1.0);

  int counts = 0;

  for (int i = 0; i < numSamples; i++) {
    double x = uniform(generator);
    double y = uniform(generator);

    if (x * x + y * y < 1.0) {
      ++counts;
    }
  }

  return counts;
}

void parallelPi3() {
  int numChunks = omp_get_max_threads();
  int chunk     = numTotalSamples / numChunks;

  int counts = 0;

  #pragma omp parallel for reduction(+ : counts)
  for (int i = 0; i < numChunks; ++i) {
    counts += samplesInsideCircle(chunk);
  }

  double approxPi = (4.0 * counts) / numTotalSamples;

  std::cout << "Parallel version 3:\n";
  std::cout << "number of samples: " << numTotalSamples << "\n";
  std::cout << "real Pi: 3.141592653589...\n";
  std::cout << "approx.: " << std::setprecision(12) << approxPi << "\n";
}

int main() {
  using millis = std::chrono::milliseconds;
  using std::chrono::duration_cast;
  using std::chrono::steady_clock;

  auto t_seq_1 = steady_clock::now();
  sequentialPi();
  auto t_seq_2 = steady_clock::now();

  auto time1 = duration_cast<millis>(t_seq_2 - t_seq_1).count();
  std::cout << "Time: " << time1 << " milliseconds.\n\n";

  auto t_par_1 = steady_clock::now();
  parallelPi();
  auto t_par_2 = steady_clock::now();

  auto time2 = duration_cast<millis>(t_par_2 - t_par_1).count();
  std::cout << "Time: " << time2 << " milliseconds.\n\n";

  auto t_parv2_1 = steady_clock::now();
  parallelPi2();
  auto t_parv2_2 = steady_clock::now();

  auto time3 = duration_cast<millis>(t_parv2_2 - t_parv2_1).count();
  std::cout << "Time: " << time3 << " milliseconds.\n\n";

  auto t_parv3_1 = steady_clock::now();
  parallelPi3();
  auto t_parv3_2 = steady_clock::now();

  auto time4 = duration_cast<millis>(t_parv3_2 - t_parv3_1).count();
  std::cout << "Time: " << time4 << " milliseconds.\n\n";
}
