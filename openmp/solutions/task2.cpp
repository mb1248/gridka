#include "omp.h"
#include <chrono>
#include <cstdio>
#include <thread>
#include <vector>
#include <string>

using namespace std::chrono_literals;

int main() {
  const int N = 100;

  int max_threads = omp_get_max_threads();
  std::vector<int> iterations(max_threads); // create a list of N integers
  std::vector<std::string> iter(max_threads, std::string(N,' ')); // create a list of N strings, each containing N spaces

  #pragma omp parallel for schedule(runtime)
  for (int i = 0; i < N; ++i) {                 // the loop iterations should be split among the threads
    ++iterations[omp_get_thread_num()];         // count how many iterations where done by each thread
    iter[omp_get_thread_num()][i] = '*';        // visualize at which iteration the thread was active
    std::this_thread::sleep_for(2ms);           // do some work
  }

  for (int i = 0; i < max_threads; i++) {
    std::printf("%4d: %4d [%s]\n", i, iterations[i], iter[i].c_str());
  }
}