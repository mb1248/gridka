#include <cstdio>
#include "omp.h"

int main() {
  using namespace std;
  
  #pragma omp parallel
  {
    printf("Block 1a Thread = %d\n", omp_get_thread_num());

    if (0 == omp_get_thread_num()) {
      printf("Number of threads = %d, task %d \n", omp_get_num_threads(), omp_get_thread_num());
    }

    printf("Block 1b Thread = %d\n", omp_get_thread_num());
  }

  printf("\nJoin\n\n");

  #pragma omp parallel
  {
    printf("Block 2a Thread = %d\n", omp_get_thread_num());

    #pragma omp single
    {
      printf("Number of threads = %d, task %d \n", omp_get_num_threads(), omp_get_thread_num());
    }

    printf("Block 2b Thread = %d\n", omp_get_thread_num());
  }

  printf("\nJoin\n\n");

  #pragma omp parallel
  {
    printf("Block 3a Thread = %d\n", omp_get_thread_num());

    #pragma omp single nowait
    {
      printf("Number of threads = %d, task %d \n", omp_get_num_threads(), omp_get_thread_num());
    }

    printf("Block 3b Thread = %d\n", omp_get_thread_num());
  }
}