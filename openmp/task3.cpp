#include <chrono>
#include <iostream>
#include <cinttypes>
#include "omp.h"

int main() {
  using millis = std::chrono::milliseconds;
  using std::chrono::duration_cast;
  using std::chrono::steady_clock;

  std::uint64_t n = 1'000'000'000; // one billion
  std::uint64_t sum = 0;

  auto start = steady_clock::now();

  for (std::uint64_t i = 1; i <= n; ++i) {
    sum += i;
  }

  auto end = steady_clock::now();

  auto time1 = duration_cast<millis>(end - start).count();
  std::cout << sum << " Time: " << time1 << " milliseconds.\n";
}
