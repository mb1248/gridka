#include <iostream>
#include <cstdio>
#include "omp.h"

int main() {
  #pragma omp parallel
  {
    std::cout << "Thread = " << omp_get_thread_num() << "\n";

    if (0 == omp_get_thread_num()) {
      std::cout << "Number of threads = " << omp_get_num_threads()
                << "(Thread = " << omp_get_thread_num() << ")\n";
    }
  }
}
