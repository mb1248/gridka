#include <chrono>
#include <iomanip>
#include <iostream>
#include <limits>
#include <random>
#include <string>
#include <vector>
#include <omp.h>

const int numTotalSamples = 10'000'000; // 10 million

unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
std::mt19937 generator(seed);                            // initialize
std::uniform_real_distribution<double> uniform(0.0,1.0); // the random number
                                                         // generator

void sequentialPi() {
  int counts = 0;
  for (int i = 0; i < numTotalSamples; ++i) {
    double x = uniform(generator); // generate a random number [0,1]
    double y = uniform(generator); // generate a random number [0,1]

    if (x * x + y * y < 1.0) {
      ++counts;
    }
  }

  double approxPi = (4.0 * counts) / numTotalSamples;

  std::cout << "Sequential version:\n";
  std::cout << "number of samples: " << numTotalSamples << "\n";
  std::cout << "real Pi: 3.141592653589...\n";
  std::cout << "approx.: " << std::setprecision(12) << approxPi << "\n";
}

int main() {
  using millis = std::chrono::milliseconds;
  using std::chrono::duration_cast;
  using std::chrono::steady_clock;

  auto start = steady_clock::now();
  sequentialPi();
  auto end = steady_clock::now();

  auto time1 = duration_cast<millis>(end - start).count();
  std::cout << "Time: " << time1 << " milliseconds.\n\n";
}
