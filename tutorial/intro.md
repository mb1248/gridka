# Welcome to "Parallel Programming with OpenMP and MPI"

## Who are we?

## Overview
* Short introduction to parallel computing
* Introduction to OpenMP
* OpenMP tasks
* Introduction to MPI
* MPI tasks

# Parallel Computing – A short introduction

## Why Parallel Computing?
* Physical boundaries imply a limit in the speed of a single CPU.
* The problems are larger and need more detailed solution in shorter time.

**Solution:** HPC (high-performance computing) systems, parallel computers

## What is Parallelism?
Distribute the workload and perform operations concurrently

### Characteristics
* Degree of parallelism: How much of the work can be done concurrently?
* Dependencies: Is anyone waiting for results from someone else?
* Load balance: Does everyone need the same time for their work?

### Example: Execution Flow Diagram
|![](images/f1.png)|
|:--:|
|(Picture: Bert van Dijk cc:by-nc-sa2)|

## Type of parallelism
### Low Dependencies
**Data parallelism**:

* Single Program Multiple Data

**Task parallelism**:

* Multiple Program Single Data
* Multiple Program Multiple Data

### High Dependencies
not so simple

## Modern Machines
* have nodes with a number of CPU cores that can access shared memory
* multiple nodes form a supercomputer, memory between nodes is distributed

### Parallel Programming Models

* **OpenMP** for shared memory
* **MPI** for distributed memory

Later more

## Performance – Where are the limits?
Performance Measurement in Flop/s: 1 **Flo**ating **p**oint operation per clock on a 2GHz CPU results in 2 GFlop/s

Modern CPU's can do more than one operation per cycle, the Haswell can do up to 16 Flop/s per clock. This come from parallelism within the CPU
* Vectors (AVX)
* FMA: fused multiply add
* 2 FMA on two ports parallel

This must be taken into account to determine the peak performance.

## Scaling
### Strong Scaling
Compare the time it takes to solve a problem with one process `T1` to the time `Tn` it takes to solve the same problem with `n` processes.

Speed up: `S(n) := T1/Tn`

**Amdahl's Law** says for a given time `T=ts+tp`, some time is spent doing operations that have to be performed in serial `ts` and some that can be performed in parallel `tp`. The speed up is

```
S(n) = T/(ts + tp/n)
```

If 95% of the code can be run in parallel, and only 5% of the code has to be performed in serial, it is hard to reach a speed up of 20 even with 4096 processes.

|![](images/Amdahl.png)|
|:--:|
|(Picture: Wikipedia Daniels220 cc:by-sa 3.0)|

### Weak Scaling
Increase the problem size to keep the amount of work per process constant.

Speed up: `S(n) := n T1/Tn`

**Gustafson Law** says the speed up is the `ts` serial part plus the scaling `tp` parallel part.

```
S(n) = ts + n tp
```

Here, a overhead due to parallel communications is neglected. In the best case the overhead is constant (`ts`). In the worst cases the overhead is increasing with the number of processes.

# Technical Things

* We have VMs for every participant. For the IP and password -> see paper.
* The user name is gks

If you want to work on the VM, connect to it via
```sh
ssh gks@ip
```

Please clone the repository containing the tasks

```sh
git clone https://gitlab.com/mb1248/gridka.git
```

* This slides are in the `tutorial` folder.
* `openmp` contains the OpenMP tasks
* `mpi` contains the MPI tasks
* Both have a `solution` folder with solutions, use this **later** at home.

# Let's start

* [OpenMP Introduction](openmp/intro.md)
* [MPI Introduction](mpi/intro.md)
