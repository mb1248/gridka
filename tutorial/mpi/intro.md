# MPI

## What is MPI?

* MPI stands for *Message Passing Interface*.
* MPI is a specification of message passing libaries.
* MPI primarily addresses the message-passing parallel programming model: data is moved from the address space of one process to that of another process through cooperative operations on each process.

## MPI Programming Model
* Designed for *distributed memory*.

    |![MPI](../images/distributed_mem.gif)|
    |:--:|
    |(Picture: Blaise Barney, Lawrence Livermore National Laboratory)|

* MPI implementation adapted shared memory (on one node)

    |![MPI-Hybrid](../images/hybrid_mem.gif)|
    |:--:|
    |(Picture: Blaise Barney, Lawrence Livermore National Laboratory)|

* However, the programming model clearly remains a distribuded memory model.

## MPI Overview
* A gerneral MPI program uses the MPI header `#include "mpi.h"`.
* Initializes the MPI environment `MPI_Init(&argc, &argv)`
* Does work and makes message passing calls
* Terminates the MPI environment `MPI_Finalize()`
* MPI-3 removes the C++-binding
* Typical C-binding `rc = MPI_Xxxxx(parameter, ...)` `rc` is the return code `MPI_SUCCESS` if successfull. (Else, it aborts by default.)

## Communicators, Groups and Rank

* MPI uses communicators and groups to define which processes may communicate with each other.
* MPI routines usually require a communicator as an argument.
* For now, we simply use `MPI_COMM_WORLD`, it is the predefined communicator that includes all of your MPI processes.
* Within a communicator, every process has its own unique, integer identifier: the rank. Ranks are contiguous and begin at zero.


## Tasks
* [Task0: First MPI Program](task0.md)
* [Task1: Send - Recv](task1.md)
* [Task2: Non-blocking](task2.md)
* [Task3: Collective Communications](task3.md)
* [Task4: All to - Communication](task4.md)
* [Task5: Hybrid Parallelization](task5.md)
