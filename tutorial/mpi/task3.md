# Task3: Collective Communications
Open _mpi/task3.cpp_ and review the code.

Until now we have looked at peer-to-peer communication, where two ranks are partners in one communication.

Communication where all (or a group of) ranks have to communicates are collective communications.

|![collective communications](../images/collective_comm.gif)|
|:--:|
|(Picture: Blaise Barney, Lawrence Livermore National Laboratory)|

## broadcast
```c++
// Broadcasts a message from the process with rank root to all other processes of the group.
int MPI_Bcast(void *buffer, int count, MPI_Datatype datatype,
    int root, MPI_Comm comm);
```
where `root` is the rank of the sender.

## scatter
```c++
// Sends data from one task to all tasks in a group.
int MPI_Scatter(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
    void *recvbuf, int recvcount, MPI_Datatype recvtype, int root,
    MPI_Comm comm);
```
does the same as broadcast but sends specific data to each rank. Therefore, `sendbuf` has the size `recvbuf` * `ranks`. However, `recvcount` are the elements received from one partner.

## gather
```c++
// Gathers values from a group of processes.
int MPI_Gather(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
    void *recvbuf, int recvcount, MPI_Datatype recvtype, int root,
    MPI_Comm comm);
```
is the reverse operation to MPI_scatter. Each rank sends the `sendbuf` to one big `recvbuf` on the `root` task.

## reduce
```c++
// Reduces values on all processes within a group.
int MPI_Reduce(const void *sendbuf, void *recvbuf, int count,
               MPI_Datatype datatype, MPI_Op op, int root,
               MPI_Comm comm);
```
where `op` is one of the following predefined reduce operations.

| MPI_Op     | Meaning                 |
| ---------  | ----------------------- |
| MPI_MAX    |  maximum                |
| MPI_MIN    |  minimum                |
| MPI_SUM    |  sum                    |
| MPI_PROD   |  product                |
| MPI_LAND   |  logical and            |
| MPI_BAND   |  bit-wise and           |
| MPI_LOR    |  logical or             |
| MPI_BOR    |  bit-wise or            |
| MPI_LXOR   |  logical xor            |
| MPI_BXOR   |  bit-wise xor           |
| MPI_MAXLOC |  max value and location |
| MPI_MINLOC |  min value and location |

Tasks:

1. Use a collective operation (but not reduce) to collect the counts and calculate pi on one process.
1. Now, you can use Reduce to calculate pi.
1. Can you send the result to all processes?

[Next task](task4.md)
