# Task4: All to - Communications
Open _mpi/task4.cpp_ and review the code.

Until now we used peer-to-peer communication and in the last task all peers to one rank.

A group of collective communication can also handle all to all communications.

|![collective communications](../images/allgather.png)![collective communications](../images/Alltoall.png)|
|:--:|
|**MPI_Allgather and MPI_Alltoall**|

## Allgather
```c++
// Gathers data from all processes and distributes it to all processes.
int MPI_Allgather(const void *sendbuf, int  sendcount,
     MPI_Datatype sendtype, void *recvbuf, int recvcount,
     MPI_Datatype recvtype, MPI_Comm comm);
```
It is similar to `MPI_Gather`, except that all processes receive the result

## Alltoall
```c++
// All processes send data to all processes.
int MPI_Alltoall(const void *sendbuf, int sendcount,
    MPI_Datatype sendtype, void *recvbuf, int recvcount,
    MPI_Datatype recvtype, MPI_Comm comm);
```
Each process breaks up its local `sendbuf` into n blocks - each containing `sendcount` elements of type `sendtype` - and divides its `recvbuf` similarly according to `recvcount` and `recvtype`. Process j sends the k-th block of its local `sendbuf` to process k, which places the data in the j-th block of its local `recvbuf`. The amount of data sent must be equal to the amount of data received, pairwise, between every pair of processes.

## Allreduce
```c++
// Reduces values from all processes and distributes the result back to all processes.
int MPI_Allreduce(const void *sendbuf, void *recvbuf, int count,
                  MPI_Datatype datatype, MPI_Op op, MPI_Comm comm);
```
Same as `MPI_Reduce` except that the result appears in the receive buffer of all the group members.

Tasks:

1. Calculate pi from the results of all processes on each process.
1. Can you solve the task without using `MPI_Allreduce`?
2. Do you see a problem?
1. Implement a version using `MPI_Allreduce`.

[Next task](task5.md)
