# Task2: Non-blocking Send – Recv
Open _mpi/task2.cpp_ and review the code.

Starting from the solution of the last task: Instead of one master that collects the results, each rank should exchange data with its neighbors.

|![](../images/scroll.png)|
|:--:|
|**see board**|

Look at the following chain of ranks.
* For simplification we only transfer data in one direction: rank A -> rank B -> ... -> rank N -> rank A (with N processes)
* If we use N-1 rounds to send the data accoring to this chain, the result from A in the first round is sent to B. In the second round B sends the received result to C and so on. Therefore, each process can get the total sum of the counts and calculate pi.

## Deadlock
Until now, we used blocking Send and Recv. This means, that the functiosn (`MPI_Recv` and `MPI_Recv`) return after a successful send or receive. If two ranks wait for a receive but the matching send is not yet processed, the program will hang. This is called deadlock.

## non-blocking
MPI also provides non-blocking versions of the functions `MPI_Ixxxx`:

```c++
// Starts a standard-mode, nonblocking send.
int MPI_Isend(const void *buf, int count, MPI_Datatype datatype, int dest,
    int tag, MPI_Comm comm, MPI_Request *request);

// Starts a standard-mode, nonblocking receive.
int MPI_Irecv(void *buf, int count, MPI_Datatype datatype,
        int source, int tag, MPI_Comm comm, MPI_Request *request);
```
Here `request` is a pointer to an `MPI_Request` structure.

### Progress check
To check the progress of one of these operations, the following function is used:
```c++
int MPI_Test (MPI_Request* request, int* flag, MPI_Status* status);
```
Where `flag=1` or `0` is set, depending on whether the operation is completed or still in progress.

### Blocking wait
To wait for an MPI_Isend or MPI_Irecv operation, the following operation is used:
```c++
int MPI_Wait (MPI_Request* request, MPI_Status* status);
```


Tasks:

1. Build the program with send/recv as simple as possible. Compare with _mpi/task2.cpp_. Does it run?
2. Beware of deadlocks. Fix the program using `MPI_Send` and `MPI_Recv`. If you need to have even or odd number of processes, that is okay. Make sure, that each rank has the same pi after the exchange.
1. Rewrite the program using `MPI_Isend` and/or `MPI_Irecv`.

[Next task](task3.md)
