# Task1: Send – Recv
Open _mpi/task1.cpp_ and review the code.

For the next tasks we use the approximate pi example from the previous OpenMP task.

```c++
int main(int argc, char* argv[]) {
  ...

  int localSampels = 1'000'000; // one million

  int counts = samplesInsideCircle(localSampels);

  ...
}
```

* `samplesInsideCircle` counts the samples on each rank.
* Now we have to collect the samples from the ranks and sum them up to calculate a better approximation of pi.
* One way to do this is a master–worker concept, where each rank is a worker that calculates the samples.
  One rank (usually rank 0) is the master and collects the interim result.
  The workers send the interim results and the master receives them.

|![](../images/scroll.png)|
|:--:|
|**see board**|

```c++
//  Performs a standard-mode blocking send.
int MPI_Send(const void *buf, int count, MPI_Datatype datatype, int dest,
    int tag, MPI_Comm comm);
```

Here `buf` is a pointer to the memory to be sent, and `count` the number of elements. The elements are of the `datatype`. MPI provides a list of standard datatypes

| MPI datatype      | C equivalent      |
|-------------------|-------------------|
| MPI_BYTE          | char              |
| MPI_INT           | int               |
| MPI_LONG          | long int          |
| MPI_UNSIGNED_CHAR | unsigned char     |
| MPI_UNSIGNED      | unsigned int      |
| MPI_UNSIGNED_LONG | unsigned long int |
| MPI_FLOAT         | float             |
| MPI_DOUBLE        | double            |

`dest` is the rank of the destination process. `tag` is an MPI-tag, you can choose this to distinguish between several MPI-communications. Two partners have to use the same MPI-tag when communicating. `comm` is the communication group.

For the `MPI_Send` we need a matching counterpart:

```c++
// Performs a standard-mode blocking receive.
int MPI_Recv(void *buf, int count, MPI_Datatype datatype,
    int source, int tag, MPI_Comm comm, MPI_Status *status);
```

The parameter are the same as before. `source` is the rank of the sending process. `status` is a pointer to an `MPI_Status` structure, that holds the rank of the sender, the tag of the message and the length of the message. If these information are not needed use `MPI_STATUS_IGNORE`.

For small messages you can send and after that receive the data. MPI has a buffer where the messages can be held until the matching receive collects them. Depending on the number of messages and the size of the messages you cannot assume that a message is first send to the buffer. Therefore, it is a good practice to first "open" a receive to provide the memory and then send.

Tasks:

1. Call the function `samplesInsideCircle` on each process and compute the approximation of pi on each process.
1. Let the worker processes send their result `count` to the master process.
1. The master collects all counts and calculates a better approximation of pi.

[Next task](task2.md)
