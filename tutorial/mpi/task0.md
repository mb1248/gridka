# Task0: First MPI Program
Open _mpi/task0.cpp_ and review the code.

```c++
#include "mpi.h"

int main(int argc, char* argv[]) {
  // Initialize the MPI environment

  // Get the number of processes
  int size;

  // Get the rank of the process
  int rank;

  // Print out a hello world message from each rank
  // one rank should print the number of ranks.

  // Finalize the MPI environment
}
```

* include the header `mpi.h` when using MPI
* Initialize and Finalize the MPI environment:

```c++
// Determines the rank of the calling process in the communicator.
int MPI_Comm_rank(MPI_Comm comm, int *rank);
```

```c++
// Returns the size of the group associated with a communicator.
int MPI_Comm_size(MPI_Comm comm, int *size);
```

Details and further commands can be found on [Open MPI v3.1.1 documentation](https://www.open-mpi.org/doc/current/).

## Compiling

```sh
mpic++ task0.cpp
```

`mpic++` is the open MPI C++ wrapper compiler.
It passes its arguments to the underlying C++ compiler along with the `-I`, `-L` and `-l` options required by Open MPI programs.

```sh
mpirun -n 4 ./a.out
```

starts the program with four processes. This, means that that the application requires four times the memory.

## Tasks:

1. Get the `rank` of each process and the `size` of the group (total number of processes).
1. Print `rank` and `size`.
1. Use `MPI_Barrier(MPI_Comm comm)` to synchronize the MPI processes
1.  print the `size` of the group afterward.

[Next task](task1.md)
