# Task5: Hybrid Parallelization
Open _mpi/task5.cpp_ and review the code.

We have learned how to use shared memory parallelization with OpenMP and distributed memory parallelization with MPI. In high-performance computing systems, single nodes usually have dozens of cores. With one MPI task per core it is easy to see that the number of communications is very high for alltoall communications. This can be reduced by using one MPI-task on a node and OpenMP threads within a node.

Tasks:

1. Write a hello world program that says hello from the n-th OpenMP threads on the m-th MPI-task.
    Compile with
    ```sh
    mpic++ task5.cpp -fopenmp
    ```

    and run with
    ```sh
    OMP_NUM_THREADS=3 mpirun -n 4 ./a.out
    ```
1. Write a program that uses OpenMP threads and MPI-task to calculate pi.

