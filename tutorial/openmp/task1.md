# Task1: Sections – different threads do different things
Open _openmp/task1.cpp_ and review the code.

```c++
int main() {
  #pragma omp parallel sections
  {
    #pragma omp section
    function_1();

    #pragma omp section
    function_2();
  }
}
```

* The `#pragma omp parallel section`s create a team of threads executing the code within this section concurrently.
* Each `#pragma omp section` assigns exactly one thread to execute the code within the section.

Tasks:

1. Run with 2 threads: `OMP_NUM_THREADS=2 ./a.out`
1. Do you see any Problems?
1. Add a third section with a third function: same as other two functions, doing 4 iterations and waiting for 250ms.

[Next task](task2.md)
