# Task0: First OpenMP program
Open _openmp/task0.cpp_ and review the code.

```c++
#include <iostream>
#include "omp.h"

int main() {
  #pragma omp parallel
  {
    std::cout << "Thread = " << omp_get_thread_num() << "\n";

    if (0 == omp_get_thread_num()) {
      std::cout << "Number of threads = " << omp_get_num_threads()
                << "(Thread = " << omp_get_thread_num() << ")\n";
    }
  }
}
```

* Include the header `omp.h` when using OpenMP functions.
* The `#pragma omp parallel` creates a team of threads all concurrently executing the code inside this region.
* The program prints the thread number of each thread.
* If the thread number is equal to zero, the program (only the master process) prints the number of threads in the team.

##Compile with
```sh
clang++ task0.cpp -fopenmp
```
or
```sh
g++ task0.cpp -fopenmp
```
and run the program with
```sh
./a.out
```
The output of the program might look like this:

```sh
$ ./a.out
Thread = 2
Thread = 0
Number of threads = 4(Thread = 0)
Thread = 1
Thread = 3
```
or this
```sh
$ ./a.out
Thread = Thread = 0
Number of threads = 4(Thread = 0)
Thread = 1
Thread = 2
3
```

We have a so called **data race**. OpenMP does not automatically figure out that multiple threads access `std::cout`, which is *thread-safe* but only if used with one `operator<<`. Therefore, the order of the output in this program is not defined.

Tasks:

1. To avoid data races, use printf instead of cout for printing.
1. The environment variable `OMP_NUM_THREADS` can be use to specify the number of parallel threads. Start the program for example with `OMP_NUM_THREADS=3 ./a.out`
1. Edit the program so that it does the following (all within a single `#pragma omp parallel` block):
   * First, using printf, let each thread print its ID,
   * then, only one thread should print the total number of threads. Print also the ID of that thread (use an if clause like in the example above).
   * Then, let each thread print its ID again.
1. Compile your program and check that it runs correctly.
1. After the parallel block, print the message `Join`.
1. Copy the parallel block and use the `#pragma omp single` instead of the if condition.
1. OpenMP sets explicite barriers, when the threads join at end of sections (like `parallel` and `single` sections). Can you see the effect of the barriers?
1. You can use `#pragma omp single nowait` to avoid this barrier (threads will not wait at the end of the `single` region). Copy the parallel region again, this time using a `single` region within it without barriers. Can you see the difference?

[Next task](task1.md)
