# OpenMP

## What is OpenMP?

* OpenMP stands for *Open Multi-Processing*.
* It is an Application Program Interface (API) used for explicit *multi-threaded, shared memory* parallelism.

## OpenMP Programming Model
* Designed for multi-processor/core, shared memory machines.
* For Uniform and Non-Uniform Memory Access (UMA/NUMA)

    |![](../images/shared_mem.gif)|
    |:--:|
    |**UMA - Uniform Memory Access** (Picture: Blaise Barney, Lawrence Livermore National Laboratory)|

    |![](../images/numa.gif)|
    |:--:|
    |**NUMA - Non-Uniform Memory Access** (Picture: Blaise Barney, Lawrence Livermore National Laboratory)|


* Thread Based Parallelism
    * A thread of executions (the work) is sheduled by the operating system.
    * Typically, the number of threads match the number of machine processors/cores.
* Explicit Parallelism
    * no automatic parallelism.
    * offering the programmer full control over parallelization.
    * Parallelization can be as simple as taking a serial program and inserting compiler directives...
    * Or as complex as inserting subroutines to define multiple levels of parallelism.
* Fork--Join Model

    |![](../images/fork_join2.gif)|
    |:--:|
    |**Fork–Join Model** (Picture: Blaise Barney, Lawrence Livermore National Laboratory)|

    * All OpenMP programs begin as a single thread: the master thread. The master thread executes sequentially until the first parallel region construct is encountered.
    * *FORK:* the master thread then creates a team of parallel threads.
    * The statements in the program that are enclosed by the parallel region construct are then executed in parallel among the various team threads.
    * *JOIN:* When the team threads complete the statements in the parallel region construct, they synchronize and terminate, leaving only the master thread.

## OpenMP API Overview
There are three ways to use OpenMP's functionalities:

* OpenMP's API provides a set of functions like ordinary C/C++ functions.
  They all start with `omp_`. For example: `omp_get_thread_num()`, which returns the identification number of the current thread.
* The second way to use the OpenMP is via environmental variables. They all start with `OMP_`. An example is `OMP_NUM_THREADS`, which sets the number of threads the program can create.
* The last way is to use the so-called pragmas.
  They all start with `#pragma omp`.

  An example is
  ```c++
  #pragma omp parallel
  {
    // structured block
  }
  ```
  which starts parallel execution of the structured block.

* [Task0: First OpenMP Programm](task0.md)
* [Task1: Sections](task1.md)
* [Task2: Parallel for I](task2.md)
* [Task3: Parallel for II](task3.md)
* [Task4: Calculation of Pi](task4.md)
