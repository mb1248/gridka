# Task2: Parallel for – different schedulers
Open _openmp/task2.cpp_ and review the code.

```c++
int main() {
  const int N = 100;

  int max_threads = omp_get_max_threads();
  std::vector<int> iterations(max_threads); // create a list of N integers
  std::vector<std::string> iter(max_threads, std::string(N,' ')); // create a list of N strings, each containing N spaces

  #pragma omp parallel for schedule(static)
  for (int i = 0; i != N; ++i) {                 // the loop iterations should be split among the threads
    ++iterations[omp_get_thread_num()];          // count how many iterations where done by each thread
    iter[omp_get_thread_num()][i] = '*';         // visualize at which iteration the thread was active
    std::this_thread::sleep_for(2ms);            // do some work
  }

  for (int i = 0; i < max_threads; ++i) {
    std::printf("%4d: %4d [%s]\n", i, iterations[i], iter[i].c_str());
  }
}
```

The `#pragma omp parallel for [clauses]` is short for
```c++
#pragma omp parallel
{
  #pragma omp for
  for(int i = 0; test; increment) {
    ...
  }
}
```
This leads to the loop being executed in parallel.

## Canonical loop form
In order to be executed in parallel, the loop must be written in a canocial form:
* `test` expression has to use one of the following operators
    * `<`,
    * `<=`,
    * `>`,
    * `>=`.
* `increment` expression has to be one of the following
    * `++var`,
    * `var++`,
    * `--var`,
    * `var--`,
    * `var += incr`,
    * `var -= incr`,
    * `var = var + incr`,
    * `var = var - incr`.

Tasks:

1. Fix the error in the code and run it.

    ```bash
    $ a.out
       0:   25 [*************************                                                                           ]
       1:   25 [                         *************************                                                  ]
       2:   25 [                                                  *************************                         ]
       3:   25 [                                                                           *************************]
    ```

2. The clause `schedule(type[, chunk_size])` changes the scheduler. Try the different types with different chunk_size

    * static
    * dynamic
    * guided
    * auto
    * runtime

    The `runtime` type can be controlled via the environment variable `OMP_SCHEDULE="dynamic, 100"`.

3. `auto` is implementation dependent. What are the default settings in `g++` and `clang++`?
1. What do you expect has the least and most overhead among the various schedulers?


[Next task](task3.md)
