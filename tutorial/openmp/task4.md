# Task4: Calculation of Pi
Open _openmp/task4.cpp_ and review the code.

|![](../images/scroll.png)|
|:--:|
|**see board**|

Pi is the area of the unit circle (with radius r = 1).
The surrounding square has an area of 4.
If we count uniformly distributed raindrops falling randomly into the circle and those falling into the surrounding square we can calculate Pi:

```
Pi = 4 * A_circle / A_square,
```

with `A_circle` the area of the circle respectively the number of drops on the circle, and `A_square` the total number of drops.

The following Monte-Carlo algorithm simulates the raindrops.

```c++
void sequentialPi() {
  int counts = 0;
  for (int i = 0; i < numTotalSamples; ++i) {
    double x = uniform(generator); // generate a random number [0,1]
    double y = uniform(generator); // generate a random number [0,1]

    if (x * x + y * y < 1.0) {
      ++counts;
    }
  }

  double approxPi = (4.0 * counts) / numTotalSamples;
}
```

Tasks:

1. Copy the code and add a second function to calculate Pi in parallel.
2. Can you speedup the solution? Think about private and shared variables, where are possible lockings or barriers?

[Next MPI](../mpi/intro.md)
