# Task3: Parallel for – Scope and Reduce
Open _openmp/task3.cpp_ and review the code.

Let us calculate the sum `1 + 2 + ... + 1000000000`

```c++
int main() {
  using millis = std::chrono::milliseconds;
  using std::chrono::duration_cast;
  using std::chrono::steady_clock;

  std::int64_t n = 1'000'000'000;  // one billion
  std::int64_t sum = 0;

  auto start = steady_clock::now();

  for (std::int64_t i = 1; i <= n; ++i) {
    sum += i;
  }

  auto end = steady_clock::now();

  auto time1 = duration_cast<millis>(end - start).count();
  std::cout << sum << " Time: " << time1 << " milliseconds.\n";
}
```
Tasks part I:

1. Run the code

    ```sh
    $ ./a.out
    500000000500000000 Time: 2822 milliseconds.
    ```

1. Parallelize the for loop as before with `#pragma omp parallel for`.

    ```sh
    $ ./a.out
    128346584797857478 Time: 4815 milliseconds.
    ```

This is wrong and even slower than without parallelization. Why?


## Data-Sharing Rules
A variable in an OpenMP parallel region can be either shared or private.

* **shared**: there exists one instance of this variable which is shared among all threads. There is no check for consistency.
* **private**: each thread has its own local copy of the variable.

### Implicit Rules
```c++
int i = 0;
int n = 10;
int a = 7;

#pragma omp parallel for
for (i = 0; i < n; i++)
{
    int b = a + i;
    ...
}
```

* Variables, which are declared outside the parallel region, are usually shared: `n`, `a`.
* However, the loop iteration variable is private: `i`.
* Variables which are declared locally within the parallel region are private: `b`.

Declaring `i` inside make it clear:
```c++
int n = 10; // shared
int a = 7;  // shared

#pragma omp parallel for
for (int i = 0; i < n; ++i) // i private
{
    int b = a + i; // b private
    ...
}
```

### Explicit Rules
The clauses

* `shared(n, a)`
* `private(b)`

can be used to explicitly set variables to shared or private.

### Reduction
For example, let us parallelize the following for loop
```c++
sum = 0;

for (int i = 0; i < 9; ++i)
{
    sum += a[i]
}
```
and let there be three threads in the team of threads. Each thread has a local sum `sumloc_`, which is a local copy of the reduction variable.
The threads should perform the following computations

+ Thread 1   `sumloc_1    = a[0] + a[1] + a[2]`
+ Thread 2   `sumloc_2    = a[3] + a[4] + a[5]`
+ Thread 3   `sumloc_3    = a[6] + a[7] + a[8]`

When joining, all local copies have to be reduced to the shared reduction variable

`sum = sumloc_1 + sumloc_2 + sumloc_3`.

We can tell this to OpenMP by writing
```c++
#pragma omp parallel for reduction(+: sum)
```
where `sum` is the reduction varible and `+` is the reduction operator, one of

+ +,
+ -,
+ *,
+ &,
+ |,
+ ^,
+ &&,
+ ||

Tasks part II:

1. What happens when you define `sum` as shared?
1. What happens when you define `sum` as private?
    `0 Time: 997 milliseconds.` Fast but not correct.

For speed we need private variables, for the correctness shared.



5. Try out the reduction with our sum.

[Next task](task4.md)
